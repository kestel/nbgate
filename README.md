nbgate [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/kestel/nbgate/badges/master/pipeline.svg)](https://gitlab.com/kestel/nbgate/commits/master)
======

nbgate is a reverse proxy to [notabenoid.org](http://notabenoid.org). It provides access to the site under a single common account while preventing the account from being hijacked.

## Docker

    docker run -it --rm --name nbgate -p 8080:8080 -e NOTAUSER=USERNAME -e NOTAPASS=PASSWORD registry.gitlab.com/kestel/nbgate

## Install

    go get -u gitlab.com/opennota/nbgate

## Run

    nbgate -u username -p password -http listenaddr (default :1337)
