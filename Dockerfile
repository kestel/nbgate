FROM golang:alpine as build

WORKDIR /go/src/nbgate
COPY *.go .

RUN go get -d -v ./...
RUN go build -v ; ls -la

FROM alpine
COPY --from=build /go/src/nbgate/nbgate /
COPY entrypoint.sh .
ENV NOTAUSER username
ENV NOTAPASS password
ENV LISTEN ":8080"
EXPOSE 8080
CMD ["/entrypoint.sh"]
